<?php

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Socialite\Facades\Socialite;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/login', function () {
    return Socialite::driver('google')->stateless()->redirect();
});
Route::get('/', function () {
    $userFromGoogle = Socialite::driver('google')->stateless()->user();
    // return json_encode($userFromGoogle->name);

    return view('selamat', ['data' => $userFromGoogle]);
});
Route::get('facebook/login', function () {
    return Socialite::driver('facebook')->stateless()->redirect();
});
Route::get('callback', function () {
    $userFromGoogle = Socialite::driver('facebook')->stateless()->user();
    return json_encode($userFromGoogle);
});
Route::get('logout', function (Request $request) {
    $request->session()->invalidate();
    $request->session()->regenerateToken();
});
Route::get('login/git', function () {
    return Socialite::driver('github')->stateless()->redirect();
});
Route::get('callback/git', function () {
    $userFromGoogle = Socialite::driver('github')->stateless()->user();
    return json_encode($userFromGoogle);
});
